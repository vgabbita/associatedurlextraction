import requests
from bs4 import BeautifulSoup

url = 'https://developer.mozilla.org/en-US/docs/Web/HTTP/Overview'
reqs = requests.get(url)
soup = BeautifulSoup(reqs.text, 'html.parser')

urls = []
for link in soup.find_all('a'):
    associated_url = link.get('href')
    if associated_url.startswith("/") or associated_url.startswith("#"):
        associated_url = url + associated_url
    urls.append(associated_url)

with open('AssociatedURLExtensions.txt', 'w') as f:
    f.write("\n".join(urls))
print("Number of Associated URLs found: ", len(urls))
